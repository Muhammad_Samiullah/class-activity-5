package com.example.classactivity5;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    ListView listView;
    String mTitle[] = {"Islamabad United", "Karachi Kings", "Lahore Qalandars", "Multan Sultans", "Peshawar Zalmi","Quetta Gladiators"};
    int images[] = {R.drawable.islamabad_united, R.drawable.karachi_kings, R.drawable.lahore_qalandars, R.drawable.multan_sultans, R.drawable.peshawar_zalmi,R.drawable.quetta_gladiators};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        listView = findViewById(R.id.listView);
        MyAdapter adapter = new MyAdapter(this, mTitle, images);
        listView.setAdapter(adapter);
    }
}

class MyAdapter extends ArrayAdapter<String> {

    Context context;
    String rTitle[];
    int rImgs[];

    MyAdapter (Context c, String title[], int imgs[]) {
        super(c, R.layout.row, R.id.textView, title);
        this.context = c;
        this.rTitle = title;
        this.rImgs = imgs;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater layoutInflater = (LayoutInflater) context.getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View row = layoutInflater.inflate(R.layout.row, parent, false);
        ImageView images = row.findViewById(R.id.imageView);
        TextView myTitle = row.findViewById(R.id.textView);
        images.setImageResource(rImgs[position]);
        myTitle.setText(rTitle[position]);
        return row;
    }
}